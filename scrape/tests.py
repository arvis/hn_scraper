from django.test import TestCase
import datetime
from . import scraping
from .models import HnTopic

class WebScrapingTests(TestCase):

    def test_can_fetch_data_from_hn(self):
        hn_data = scraping.get_data_from_hn()
        # print([*hn_data])
        # print(hn_data)
        self.assertEquals(len([*hn_data]),30)

    def test_handles_correct_url(self):
        got_data, hn_data = scraping.get_data_from_url('https://news.ycombinator.com/')
        self.assertIs(got_data, True)
        self.assertIs("<title>Hacker News</title>" in hn_data.decode("utf-8"), True)

    def test_handles_incorrect_url(self):
        got_data, hn_data = scraping.get_data_from_url('')
        self.assertIs(got_data, False)

class DbUpdateTests(TestCase):

    def test_update_hn_data(self):
        scraping.update_hn_data()
        self.assertEquals(HnTopic.objects.count(),30)


class WebAllParsingTests(TestCase):
    hn_data = None
    rows = None
    score = None

    @classmethod  
    def setUpTestData(cls):  
        got_data, cls.hn_data = scraping.get_data_from_url('https://news.ycombinator.com/')

    def test_get_all_topics(self):
        all_topics = scraping.get_all_hn_tr(self.hn_data)
        self.assertEquals(len(all_topics), 92)

    def test_split_topics(self):
        all_topics = scraping.get_all_hn_tr(self.hn_data)
        topics = scraping.split_all_table_rows(all_topics)
        self.assertEquals(len([*topics]),30)


class WebParsingTestsFromFile(TestCase):
    file_contents = None

    @classmethod  
    def setUpTestData(cls):  
        f = open("scrape/test_data/hn_test_data.html", "r")
        cls.file_contents = f.read()

    def test_can_parse_hn_topic(self):
        all_table_rows = scraping.get_all_hn_tr(self.file_contents)
        topic_data = scraping.parse_hn_topic(all_table_rows[0])
        self.assertEquals(int(topic_data["id"]), 33256378)
        self.assertEquals((topic_data["title"]), 'Starlink Aviation')

    def test_can_parse_hn_score(self):
        all_table_rows = scraping.get_all_hn_tr(self.file_contents)
        data = scraping.parse_hn_score(all_table_rows[1].find("span",class_="score"))
        self.assertEquals(int(data), 323)

    def test_can_parse_hn_time_posted(self):
        all_table_rows = scraping.get_all_hn_tr(self.file_contents)
        data = scraping.parse_hn_time(all_table_rows[1].find("span",class_="age"))
        self.assertEquals(data, datetime.datetime(2022, 10, 19, 1, 40, 46))


    def test_split_rows(self):
        all_table_rows = scraping.get_all_hn_tr(self.file_contents)
        topics = scraping.split_all_table_rows(all_table_rows)
        self.assertEquals(len([*topics]),30)
        hn_ids = [*topics]
        first_record = topics[hn_ids[0]]

        self.assertEquals(first_record['id'],hn_ids[0])
        self.assertEquals(first_record['points'],'323')
        self.assertEquals(first_record['title'],'Starlink Aviation')
        self.assertEquals(first_record['date_created'],datetime.datetime(2022, 10, 19, 1, 40, 46))

