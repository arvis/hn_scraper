from django.shortcuts import render
from django.http import JsonResponse
from .models import HnTopic


def index(request):
    topics = HnTopic.objects.all()
    return render(request, 'index.html',{'topics':topics})

def json_response(request):
    data = list(HnTopic.objects.values())  
    return JsonResponse(data, safe=False)
