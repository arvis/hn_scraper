from django.db import models

class HnTopic(models.Model):
    # (title, link, points, date created)
    hn_id = models.BigIntegerField(unique=True)
    title = models.CharField(max_length=500)
    link= models.URLField(max_length=500)
    points = models.IntegerField(null=True)
    date_created = models.DateTimeField()

    date_modified = models.DateTimeField(auto_now=True)
    date_published = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.hn_id} - {self.title}"