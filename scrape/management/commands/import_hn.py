from django.core.management.base import BaseCommand, CommandError
from scrape import scraping 


class Command(BaseCommand):
    help = 'Get Hacker news frontpage, parse topics, import to database'

    def handle(self, *args, **options):
        status = scraping.update_hn_data()
        
        status_text = 'HN import successful' if status else 'HN import failed'
        self.stdout.write(status_text)