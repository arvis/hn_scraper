import requests
import time
from bs4 import BeautifulSoup
from django.utils.dateparse import parse_datetime
from django.utils.timezone import make_aware
from .models import HnTopic


def get_data_from_hn():
    hn_url = 'https://news.ycombinator.com/'
    correct, content =  get_data_from_url(hn_url)
    if not correct:
        return {}

    all_table_rows = get_all_hn_tr(content)
    topics = split_all_table_rows(all_table_rows)
    return topics 

def update_hn_data():
    topics = get_data_from_hn()

    # if len([*topics])<30:
    #     return False

    for k in [*topics]:
        create_update_hn_topic(topics[k])
    return True

def create_update_hn_topic(topic):
    obj, created = HnTopic.objects.get_or_create(
        hn_id=int(topic['id']),
        defaults={
            'title': topic['title'],
            'link': topic['link'],
            'points': int(topic['points']),
            'date_created': make_aware(topic['date_created'])},
    )

    if not created:
        obj.points = int(topic['points'])

def get_data_from_url(url):
    try:    
        time.sleep(1)    
        response = requests.get(url)
        # check if content is valid
        if response.status_code != 200:
            return False,bytes()

        hn_data = response.content
        
        return True, hn_data
    except:
        return False,bytes()

def check_if_full_link(url):
    if url.lower().startswith("http"):
        return url
    # assuming this is internal link
    hn_url = 'https://news.ycombinator.com/'
    full_url = f"{hn_url}{url}"
    return full_url

def parse_hn_topic(t):
    id = t.get_attribute_list("id")[0]
    all_link = t.find("span",class_="titleline").find("a")    
    link = check_if_full_link(all_link.get_attribute_list("href")[0])

    title = all_link.get_text()

    topic_data = {'id':id,'link':link, 'title':title}
    return topic_data


def parse_hn_score(row):
    attrs = row.get_attribute_list("id")
    if attrs is None or len(attrs)<1:
        return ''

    id = attrs[0].split("_")[1]
    if row.get_text():
        score_text = row.get_text().split()[0]
        # return {id:score_text}
        return score_text
    else:
        return None

def parse_hn_time(row):
    # <span class="age" title="2022-10-20T21:08:22"><a href="item?id=33280719">2 hours ago</a></span>
    # TODO: check if not empty
    attrs = row.get_attribute_list("title")
    if attrs is None or len(attrs)<1:
        return None

    title_time = row.get_attribute_list("title")[0]
    parsed_time = parse_datetime(title_time)
    return parsed_time


def get_hn_table(content):
    soup = BeautifulSoup(content, 'html.parser')
    topics_table = soup.find_all('table', class_='itemlist')    
    return topics_table[0]

def get_all_hn_tr(content):
    itemlist = get_hn_table(content)
    rows = itemlist.find_all('tr')
    return rows

def split_all_table_rows(tr):

    topics = {}
    problems_tr = 0 
    for idx,t in enumerate(tr):
        
        try:
            if t.has_attr('class') and t['class'][0]=='athing':
                id = t.get_attribute_list("id")[0]

                points_span = tr[idx+1].find("span",class_="score")
                time_span = tr[idx+1].find("span",class_="age")
                
                topic = parse_hn_topic(t)
                points = None
                if points_span is not None:
                    points = parse_hn_score(points_span)

                time_created = parse_hn_time(time_span)
                topic['points'] = points
                topic['date_created'] = time_created
                topics[topic['id']] = topic
        except:
          print("An exception occurred")
          continue

    return topics
