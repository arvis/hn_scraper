# Generated by Django 4.1.2 on 2022-10-22 16:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scrape', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='hntopic',
            name='hn_id',
            field=models.BigIntegerField(default=0, unique=True),
            preserve_default=False,
        ),
    ]
