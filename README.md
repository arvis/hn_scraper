# About
This project is meant for scraping data for [Hacker news](https://news.ycombinator.com/).
## Installation
Check out git project then run. Docker and docker-compose must be installed. 
Run this command to start docker
### `docker-compose up`

To setup models go to docker shell on another tab `docker exec  -it hn_scraper_web_1 bash`
Then execute `python manage.py migrate`
 

## Scraping data
To start scraping you have to run it `docker-compose up`
Then go to docker shell on another tab `docker exec  -it hn_scraper_web_1 bash`
Then run `python manage.py test scrape`

## Testing
To test it is advised to go into docker shell `docker exec  -it hn_scraper_web_1 bash`
Then run `python manage.py test scrape`
